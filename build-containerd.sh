#!/bin/bash

#   Copyright The containerd Authors.

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#       http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#RUNC_VERSION=$(grep "opencontainers/runc" vendor.conf  | awk '{print $2}')
#CONTAINERD_VERSION=$(git log --oneline -1 --quiet | awk '{print $1}')

set -euo pipefail

# basic packages.
dnf install -y --disablerepo updates \
    @'Development Tools' \
    libbtrfs \
    libbtrfsutil \
    libseccomp \
    libseccomp-devel \
    btrfs-progs-devel \
    glibc-static \
    libseccomp-static \
    fuse \
    vim \
    findutils \
    tree \
    s3cmd
# golang
curl -o golang.tar.gz https://dl.google.com/go/go1.14.linux-amd64.tar.gz && \
tar -xf golang.tar.gz && \
mv go /usr/local/ && \
mkdir /go

BUILD_ORIGINAL_DIR=$(pwd)

export GOROOT=/usr/local/go
export GOPATH=/go
export GO111MODULE=off
export PATH=${GOPATH}/bin:${GOROOT}/bin:$PATH

git clone https://github.com/opencontainers/runc \
          $GOPATH/src/github.com/opencontainers/runc && \
cd $GOPATH/src/github.com/opencontainers/runc && \
git checkout ${RUNC_VERSION} && \
GO111MODULE=off make BUILDTAGS='seccomp apparmor' && \
GO111MODULE=off make install && \
git clone https://github.com/containerd/containerd \
          $GOPATH/src/github.com/containerd/containerd && \
cd $GOPATH/src/github.com/containerd/containerd && \
git checkout ${CONTAINERD_VERSION} && \
GO111MODULE=off make && GO111MODULE=off make install

export BUILDS=/builds-output
tree /usr/local/bin
mkdir -p $BUILDS/usr/local/bin \
&& cp /usr/local/bin/* $BUILDS/usr/local/bin \
&& mkdir -p $BUILDS/usr/local/sbin \
&& cp /usr/local/sbin/runc $BUILDS/usr/local/sbin \
&& tree $BUILDS

mkdir -p $BUILDS/etc
cat > $BUILDS/etc/crictl.yaml <<EOF
runtime-endpoint: unix:///run/containerd/containerd.sock
EOF

mkdir -p $BUILDS/etc/systemd/system
cat > $BUILDS/etc/systemd/system/containerd.service <<EOF
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target

[Service]
User=root
ExecStartPre=/sbin/modprobe overlay
ExecStart=/usr/local/bin/containerd
Restart=always
RestartSec=5
Delegate=yes
KillMode=process
OOMScoreAdjust=-999
LimitNOFILE=1048576
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNPROC=infinity
LimitCORE=infinity

[Install]
WantedBy=multi-user.target
EOF

mkdir -p $BUILDS/etc/containerd
cat > $BUILDS/etc/containerd/config.toml <<EOF
#root = "/var/lib/containerd"
#state = "/run/containerd"
#subreaper = true
#oom_score = 0

#[grpc]
#  address = "/run/containerd/containerd.sock"
#  uid = 0
#  gid = 0

#[debug]
#  address = "/run/containerd/debug.sock"
#  uid = 0
#  gid = 0
#  level = "info"
EOF

cd ${BUILD_ORIGINAL_DIR}
pwd
tree $BUILD_ORIGINAL_DIR
cd $BUILDS/
tar -czvf containerd-cern-${CONTAINERD_VERSION}-${CERN_SUFFIX}.tar.gz ./*
sha256sum containerd-cern-${CONTAINERD_VERSION}-${CERN_SUFFIX}.tar.gz
sha256sum containerd-cern-${CONTAINERD_VERSION}-${CERN_SUFFIX}.tar.gz | awk '{print $1}' > containerd-cern-${CONTAINERD_VERSION}-${CERN_SUFFIX}.tar.gz.sha256
cp containerd-cern-${CONTAINERD_VERSION}-${CERN_SUFFIX}.tar.gz ${BUILD_ORIGINAL_DIR}/
cp containerd-cern-${CONTAINERD_VERSION}-${CERN_SUFFIX}.tar.gz.sha256 ${BUILD_ORIGINAL_DIR}/
