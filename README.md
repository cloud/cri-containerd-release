# cri-containerd-release

This is a repo which only mirrors cri-containerd-releases from https://storage.googleapis.com/cri-containerd-release/ .
Related docs: https://github.com/containerd/cri/blob/9474b05dd7aedd4c7d3915b25bbf871e3dfe0459/docs/installation.md#release-tarball